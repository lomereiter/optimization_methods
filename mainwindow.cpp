#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "optimizer.hpp"
#include "point.hpp"
#include "trajectoryview.h"
#include "condition.hpp"
#include "jsfunction.hpp"

#include <QPainterPath>
#include <QPixmap>
#include <QImage>
#include <QRect>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QDebug>
#include <QtWidgets/QMessageBox>

#include <vector>
#include <algorithm>
#include <chrono>
#include <cmath>
#include <cassert>

MainWindow::MainWindow(uint width, uint height) :
    QMainWindow(NULL),
    ui(new Ui::MainWindow),
    timer_(new QTimer(this)),
    interval_(100),
    w_(width),
    h_(height),
    graphics_scene_(new QGraphicsScene(QRect(0, 0, w_, h_))),
    graphics_view_(nullptr),
    method_(NULL),
    js_engine_(),
    current_finished_(true),
    current_func_body_(""),
    current_domain_(std::vector<util::Interval>{{-1,1},{-1,1}})
{
    ui->setupUi(this);
    updateSettings();

    graphics_view_ = new TrajectoryView(graphics_scene_, ui->centralWidget);
    graphics_view_->setFixedSize(w_ + 8, h_ + 8);
    ui->verticalLayout->addWidget(graphics_view_);
    //ui->layoutWidget->setFixedSize(w_ + 24, h_ + 120);

    QComboBox* combobox = ui->optMethod;
    combobox->lineEdit()->setAlignment(Qt::AlignHCenter);
    for (int i = 0; i < combobox->count(); ++i)
        combobox->setItemData(i, Qt::AlignHCenter, Qt::TextAlignmentRole);
    combobox->setEditable(false);
    combobox->show();

    connect(ui->applySettingsButton, SIGNAL(released()), this, SLOT(updateSettings()));
    connect(graphics_view_, SIGNAL(clicked(QPoint)), this, SLOT(runNewSimulation(QPoint)));
    connect(timer_, SIGNAL(timeout()), this, SLOT(updateScene()));
}

QPixmap MainWindow::createPixmapForFunction(Function f, const Domain &d) const {
    QImage image(w_, h_, QImage::Format_ARGB32);

    const double W = image.width();
    const double H = image.height();

    std::vector<double> values(W * H);

    for (uint i = 0; i < W; ++i) {
        for (uint j = 0; j < H; ++j) {
            double alpha = (double)i / image.width();
            double beta = (double)j / image.height();
            double x = d[0].from() * alpha + d[0].to() * (1 - alpha);
            double y = d[1].from() * beta + d[1].to() * (1 - beta);
            values[i * H + j] = f(x, y);
        }
    }

    auto min_it = std::min_element(values.begin(), values.end());
    double min = *min_it;
    auto max_it = std::max_element(values.begin(), values.end());
    double max = *max_it;
    double variation = max - min;

    for (uint i = 0; i < W; ++i) {
        for (uint j = 0; j < H; ++j) {
            double value = values[i * H + j];
            uint r = 255 * (value - min) / variation;
            uint g = 255 * sqrt((value - min) / variation);
            uint b = 255 * (1 - sqrt((max - value) / variation));
            image.setPixel(W - i - 1, H - j - 1, qRgb(r, g, b));
        }
    }

    // offset = i * H + j, i < W, j < H
    auto offset = int(min_it - values.begin());
    auto min_j = int(offset - floor(offset / H) * H);
    auto min_i = int(floor((offset - min_j) / H));
    auto min_x = W - min_i - 1;
    auto min_y = H - min_j - 1;

    auto min_color = qRgb(255, 32, 32);
    for (uint i = 0; i < 8; ++i) {
        image.setPixel(min_x + i, min_y + i, min_color);
        image.setPixel(min_x - i, min_y + i, min_color);
        image.setPixel(min_x - i, min_y - i, min_color);
        image.setPixel(min_x + i, min_y - i, min_color);
    }

    return QPixmap::fromImage(image);
}

void MainWindow::setup(Function f, const Domain &d) {
    graphics_scene_->clear();

    assert(method_ != nullptr);

    method_->setFunction(f)->setDomain(d);

    ui->controlPanel->setEnabled(false);
    ui->controlPanel->repaint();
    if (graphics_view_ != nullptr)
        graphics_view_->setEnabled(false);
    auto old_title = windowTitle();
    setWindowTitle("Computing new color map, wait a few seconds...");
    qApp->processEvents();

    graphics_scene_->setBackgroundBrush(QBrush(createPixmapForFunction(f, d)));
    if (graphics_view_ != nullptr) {
        graphics_view_->show();
        graphics_view_->setEnabled(true);
    }

    ui->controlPanel->setEnabled(true);
    ui->controlPanel->repaint();
    setWindowTitle(old_title);
    qApp->processEvents();
}

void MainWindow::setup(QString func_body, const Domain &d) {
    JsFunction js_func{func_body, js_engine_};
    if (func_body != current_func_body_ || d != current_domain_)
        setup(js_func, d);
    else
        method_->setFunction(js_func)->setDomain(d);

    current_func_body_ = func_body;
    current_domain_ = d;
}

void MainWindow::setup(const char * func_body, const Domain &d) {
    setup(QString(func_body), d);
}

void MainWindow::runOptimizer(QPoint start) {
    qDebug() << start.x() << ", " << start.y();
    qDebug() << graphics_view_->x() << ", " << graphics_view_->y();
    qDebug() << graphics_view_->sceneRect().x() << ", " << graphics_view_->sceneRect().y();

    auto xrel = (double)(start.x() - graphics_view_->sceneRect().x()) / w_;
    auto yrel = (double)(start.y() - graphics_view_->sceneRect().y()) / h_;

    method_->run(xrel, yrel);

    current_finished_ = false;
    timer_->start(interval_);
}

void MainWindow::updateScene() {
    if (current_finished_)
        return;

    method_->updateGraphicsScene();

    if (method_->finished()) {
      current_finished_ = true;
      timer_->stop();

      QMessageBox::information(this, "Finished",
                               QString("x = ") + QString::number(method_->bestX()) +
                               ", y = " + QString::number(method_->bestY()) +
                               ", f(x, y) = " + QString::number(method_->bestValue()));
    }
}

void MainWindow::cleanup() {
    timer_->stop();
    if (method_ != nullptr)
        method_->clearGraphicsScene();
    graphics_scene_->clear();
    if (method_ != nullptr)
        method_->initGraphicsScene();
}

void MainWindow::updateSettings() {
    current_finished_ = true;
    cleanup();

    setUpdateInterval(ui->updateIntervalSpinBox->value());

    switch (ui->optMethod->currentIndex()) {
    case 0:
        setOptimizationMethod(std::make_shared<gui::NelderMeadOptimizationMethodRunner>());
        break;
    case 1:
        setOptimizationMethod(std::make_shared<gui::RandomSearchOptimizationMethodRunner>());
        break;
    case 2:
        throw "WTF???";
    }

    method_->initGraphicsScene();

    setup(ui->funcBody->currentText(),
          std::vector<util::Interval>{
            { ui->x1->value(), ui->x2->value() },
            { ui->y1->value(), ui->y2->value() }
          });

    std::vector<StopConditionPtr> conditions;

    using namespace optimization;
    auto timeout = std::chrono::seconds(ui->timeoutSpinBox->value());
    conditions.push_back(std::make_shared<LimitTime>(timeout));

    auto max_evals = ui->evalSpinBox->value();
    conditions.push_back(std::make_shared<LimitNumberOfEvaluations>(max_evals));

    auto max_steps = ui->stepSpinBox->value();
    conditions.push_back(std::make_shared<LimitNumberOfSteps>(max_steps));

    setStopCondition(
        std::accumulate(conditions.begin() + 1, conditions.end(), conditions.front(),
        [](StopConditionPtr cond1, StopConditionPtr cond2) -> StopConditionPtr {
            return std::make_shared<CompositeStopCondition>(cond1, cond2);
        }));
}

void MainWindow::runNewSimulation(QPoint start) {
    current_finished_ = true;
    cleanup();

    runOptimizer(start);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete graphics_view_;
    delete graphics_scene_;
    delete timer_;
}
