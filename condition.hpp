#ifndef CONDITION_HPP
#define CONDITION_HPP

#include <cstddef>
#include <tuple>
#include <utility>
#include <chrono>

#include "sequence.hpp"
#include "optimizer.hpp"

namespace optimization {

class StopCondition {

protected:
  typedef NelderMeadGenerator NMG;
  typedef NMG::State NelderMeadState;
  typedef Sequence<NelderMeadState> NelderMeadSeq;
  typedef NelderMeadSeq::ptr NelderMeadSeqPtr;
  typedef NMG::Args NelderMeadArgs;

  typedef RandomSearchGenerator RSG;
  typedef RSG::State RandomSearchState;
  typedef Sequence<RandomSearchState> RandomSearchSeq;
  typedef RandomSearchSeq::ptr RandomSearchSeqPtr;
  typedef RSG::Args RandomSearchArgs;

public:
  virtual void prepareArgs(NelderMeadArgs&) {}
  virtual void prepareArgs(RandomSearchArgs&) {}

  virtual NelderMeadSeqPtr map(NelderMeadSeqPtr args) = 0;
  virtual RandomSearchSeqPtr map(RandomSearchSeqPtr args) = 0;
};

namespace detail {

template <typename Params, typename ResultState>
class BasicStopCondition : public StopCondition {
  Params params_;

  /* ResultState shall have
  // * constructor(Params)
  // * templated onBeforePopFront(Seq &)
  // * templated isEmpty(Seq &)
  */
  template <class Gen>
  struct Result : public Sequence<typename Gen::State> {
    typedef Sequence<typename Gen::State> Seq;
    typedef typename Gen::Args Args;

    typename Seq::ptr seq_;
    ResultState result_state_;
    size_t steps_;

    Result(Params params, typename Seq::ptr seq) :
      seq_(seq), result_state_(params)
    {
    }

    virtual bool empty() {
      return seq_->empty() || result_state_.isEmpty(seq_);
    }

    virtual typename Seq::elem_type front() {
      return seq_->front();
    }

    virtual void popFront() {
      result_state_.onBeforePopFront(seq_);
      seq_->popFront();
    }
  };

public:
  BasicStopCondition(Params params) : params_(params) {}

protected:

  virtual StopCondition::NelderMeadSeqPtr
  map(StopCondition::NelderMeadSeqPtr seq) {
    return std::make_shared<Result<NelderMeadGenerator>>(params_, seq);
  }

  virtual StopCondition::RandomSearchSeqPtr
  map(StopCondition::RandomSearchSeqPtr seq) {
    return std::make_shared<Result<RandomSearchGenerator>>(params_, seq);
  }
};

template <typename Params, typename FuncState>
class IntrusiveStopCondition : public StopCondition {
  Params params_;

  struct AbortComputation {};

  struct AbortingFunc {
    Function f_;
    FuncState state_;

    AbortingFunc(const Function &f, Params params) :
      f_(f), state_(params) {}

    double operator()(const Point &u) {
      if (state_.stop())
        throw AbortComputation();
      state_.update();
      return f_(u);
    }
  };

  static Function abortingFunc(const Function &func, Params params) {
    return Function{func.dim, AbortingFunc{func, params}};
  }

  template <typename Args>
  void prepareArgsImpl(Args &args) {
    args.function = abortingFunc(args.function, params_);
  }

  virtual void prepareArgs(typename StopCondition::NelderMeadArgs &args) {
    prepareArgsImpl(args);
  }

  virtual void prepareArgs(typename StopCondition::RandomSearchArgs &args) {
    prepareArgsImpl(args);
  }

  template <class Gen>
  struct Result : public Sequence<typename Gen::State> {
    typedef Sequence<typename Gen::State> Seq;
    typedef typename Gen::Args Args;
    typename Seq::ptr seq_;

    bool empty_;
    bool init_;

    template <typename T>
    struct initialize_later {
      std::array<char, sizeof(T)> storage;
      initialize_later& operator=(const T& t) {
          T* __attribute__((may_alias)) p = reinterpret_cast<T*>(&storage);
          *p = t;
          return *this;
      }
      T& get() { return *(reinterpret_cast<T*>(&storage)); }
    };

    initialize_later<typename Seq::elem_type> front_;

    Result(typename Seq::ptr seq) :
      seq_(seq), empty_(seq_->empty()), init_(false)
    {
     if (!empty_) {
       try { front_ = seq_->front(); }
       catch (AbortComputation) { empty_ = true; }
     }
    }

    virtual bool empty() {
      return empty_;
    }

    virtual typename Seq::elem_type front() {
      return front_.get();
    }

    virtual void popFront() {
      if (seq_->empty()) {
          empty_ = true;
          return;
      }

      try {
        seq_->popFront();
        front_ = seq_->front();
      } catch (AbortComputation) {
        empty_ = true;
      }
    }
  };

public:
  IntrusiveStopCondition(Params params) : params_(params) {}

  virtual StopCondition::NelderMeadSeqPtr
  map(StopCondition::NelderMeadSeqPtr seq) {
    return std::make_shared<Result<NelderMeadGenerator>>(seq);
  }

  virtual StopCondition::RandomSearchSeqPtr
  map(StopCondition::RandomSearchSeqPtr seq) {
    return std::make_shared<Result<RandomSearchGenerator>>(seq);
  }
};

}

class CompositeStopCondition : public StopCondition {
  typedef StopCondition Base;
  typedef Base::NelderMeadArgs NelderMeadArgs;
  typedef Base::RandomSearchArgs RandomSearchArgs;
  typedef Base::NelderMeadSeqPtr NelderMeadSeqPtr;
  typedef Base::RandomSearchSeqPtr RandomSearchSeqPtr;

  std::shared_ptr<Base> cond1_, cond2_;

  template <typename Args>
  void prepareArgsImpl(Args& args) {
    cond1_->prepareArgs(args);
    cond2_->prepareArgs(args);
  }

  template <typename SeqPtr>
  SeqPtr mapImpl(SeqPtr seq) {
    return cond2_->map(cond1_->map(seq));
  }

public:
  virtual void prepareArgs(NelderMeadArgs& args) {
    prepareArgsImpl(args);
  }

  virtual void prepareArgs(RandomSearchArgs& args) {
    prepareArgsImpl(args);
  }

  virtual NelderMeadSeqPtr map(NelderMeadSeqPtr ptr) { return mapImpl(ptr); }
  virtual RandomSearchSeqPtr map(RandomSearchSeqPtr ptr) { return mapImpl(ptr); }

  CompositeStopCondition(const std::shared_ptr<StopCondition> &cond1,
                         const std::shared_ptr<StopCondition> &cond2) :
    cond1_(cond1), cond2_(cond2) {}
};

namespace detail {
  namespace limit_number_of_steps {
    struct Parameters {
      size_t max_steps;
    };

    struct ResultState {
      size_t steps;
      size_t max_steps;

      ResultState(Parameters params) : steps(0), max_steps(params.max_steps) {}

      template <typename Seq> void onBeforePopFront(Seq &) { ++steps; }
      template <typename Seq> bool isEmpty(Seq &) { return steps >= max_steps; }
    };

    typedef detail::BasicStopCondition<Parameters, ResultState> impl;
  }

  namespace limit_time {
    struct Parameters {
      std::chrono::seconds timeout;
    };

    struct FuncState {
      std::chrono::seconds timeout;
      std::chrono::system_clock::time_point start, now;

      FuncState(Parameters params) :
        timeout(params.timeout), start(std::chrono::system_clock::now()), now(start) {}

      void update() {
        now = std::chrono::system_clock::now();
      }
      
      bool stop() {
        return std::chrono::duration_cast<std::chrono::seconds>(now - start) >= timeout;
      }
    };

    typedef detail::IntrusiveStopCondition<Parameters, FuncState> impl;
  }

  namespace limit_number_of_evaluations {
    struct Parameters {
      size_t max_evals;
    };

    struct FuncState {
      size_t evals;
      size_t max_evals;
      FuncState(Parameters params) : evals(0), max_evals(params.max_evals) {}
      bool stop() { return evals > max_evals; }
      void update() { ++evals; }
    };
    
    typedef detail::IntrusiveStopCondition<Parameters, FuncState> impl;
  }
}
  
class LimitNumberOfSteps : public detail::limit_number_of_steps::impl {
public:
  LimitNumberOfSteps(size_t max_steps) :
    detail::limit_number_of_steps::impl(detail::limit_number_of_steps::Parameters{max_steps})
  {}
};

class LimitTime : public detail::limit_time::impl {
public:
  LimitTime(std::chrono::seconds max_seconds) :
    detail::limit_time::impl(detail::limit_time::Parameters{max_seconds})
  {}
};

class LimitNumberOfEvaluations : public detail::limit_number_of_evaluations::impl {
public:
  LimitNumberOfEvaluations(size_t max_evals) :
    detail::limit_number_of_evaluations::impl(detail::limit_number_of_evaluations::Parameters{max_evals})
  {}
};
 

}

#endif // CONDITION_HPP
