#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "domain.hpp"
#include "optimization_gui.hpp"
#include "condition.hpp"

#include <QtWidgets/QMainWindow>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGraphicsPathItem>
#include <QPainterPath>
#include <QTimer>
#include <QPoint>
#include <QtScript/QScriptEngine>

#include <functional>

namespace Ui { class MainWindow; }

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(uint width, uint height);
    ~MainWindow();

    void setOptimizationMethod(gui::OptimizationMethodRunnerPtr method) {
      if (method_ != NULL) {
        method_->clearGraphicsScene();
      }
      method_ = method;
      method_->setStopCondition(stop_condition_);
      method_->setGraphicsScene(graphics_scene_);
      method_->initGraphicsScene();
    }

    typedef gui::OptimizationMethodRunner::Function Function;
    typedef gui::OptimizationMethodRunner::Domain Domain;
    typedef gui::OptimizationMethodRunner::StopConditionPtr StopConditionPtr;

    void setup(Function func, const Domain &d);
    void setup(QString func, const Domain &d);
    void setup(const char * func, const Domain &d);

    void setUpdateInterval(uint msec) {
      interval_ = msec;
    }

    void setStopCondition(StopConditionPtr stop_condition) {
      stop_condition_ = stop_condition;
      method_->setStopCondition(stop_condition_);
    }

public slots:
    void runOptimizer(QPoint start);

private:
    Ui::MainWindow *ui;
    QTimer *timer_;
    uint interval_;

    StopConditionPtr stop_condition_;

    uint w_;
    uint h_;

    QGraphicsScene *graphics_scene_;
    QGraphicsView *graphics_view_;

    gui::OptimizationMethodRunnerPtr method_;

    QScriptEngine js_engine_;

    bool current_finished_;
    QString current_func_body_;
    Domain current_domain_;

    QPixmap createPixmapForFunction(Function f, const Domain &d) const;
    QPen getCurrentSegmentPen() const;

    void cleanup();

private slots:
    void runNewSimulation(QPoint start);
    void updateSettings();
    void updateScene();
};

#endif // MAINWINDOW_H
