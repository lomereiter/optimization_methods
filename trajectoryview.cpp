#include "trajectoryview.h"

#include <QMouseEvent>
#include <QPainter>

TrajectoryView::TrajectoryView(QGraphicsScene *scene, QWidget *parent) :
    QGraphicsView(scene, parent)
{
    setRenderHints(QPainter::Antialiasing);
}

void TrajectoryView::mousePressEvent(QMouseEvent *event) {
    QGraphicsView::mousePressEvent(event);
    emit clicked(event->pos());
}
