#-------------------------------------------------
#
# Project created by QtCreator 2013-10-15T21:20:37
#
#-------------------------------------------------

QT       += core gui script widgets

TARGET = optimization
TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++0x

SOURCES += main.cpp\
        mainwindow.cpp \
    trajectoryview.cpp

HEADERS  += mainwindow.h \
    optimizer.hpp \
    point.hpp \
    sequence.hpp \
    jsfunction.hpp \
    domain.hpp \
    trajectoryview.h \
    optimization_gui.hpp \
    condition.hpp \
    state_sequence_factory.hpp

FORMS    += mainwindow.ui
