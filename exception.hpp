#ifndef __EXCEPTION_HPP__
#define __EXCEPTION_HPP__

#include <stdexcept>

class DimensionMismatchException : public std::runtime_error {
public:
  DimensionMismatchException(std::string msg) : std::runtime_error(msg) {}
};


class ZeroDimensionException : public std::runtime_error {
public:
  ZeroDimensionException() : std::runtime_error("Zero dimensions are not allowed") {}
};

#endif
