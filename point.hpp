#ifndef POINT_HPP
#define POINT_HPP

#include "exception.hpp"

#include <vector>
#include <cstddef>
#include <iostream>

namespace util {

class Point {
  std::vector<double> coords_;

  friend void enforceSameDimensions(const Point&, const Point&);
  friend Point operator+(const Point&, const Point&);
  friend Point operator-(const Point&, const Point&);
  friend Point operator-(const Point&);
  friend Point operator*(double, const Point&);
  friend Point operator/(const Point&, double);

public:
  Point(size_t dim) : coords_(dim) {
    if (dim == 0)
      throw new ZeroDimensionException();
  }

  Point(const std::vector<double> &coords) : coords_(coords) {
    if (coords.size() == 0)
      throw new ZeroDimensionException();
  }

  size_t dim() const {
    return coords_.size();
  }

  double operator[](size_t index) const {
    return coords_[index];
  }

  double& operator[](size_t index) {
    return coords_[index];
  }
};

inline void enforceSameDimensions(const Point &a, const Point &b) {
  if (a.dim() != b.dim())
    throw new DimensionMismatchException("dimensions mismatch");
}

inline Point operator+(const Point& a, const Point& b) {
  enforceSameDimensions(a, b);
  std::vector<double> result(a.dim());
  for (size_t i = 0; i < a.dim(); ++i)
    result[i] = a[i] + b[i];
  return Point(result);
}

inline Point operator-(const Point& a, const Point& b) {
  return a + (-b);
}

inline Point operator-(const Point& a) {
  std::vector<double> result(a.dim());
  for (size_t i = 0; i < a.dim(); ++i)
    result[i] = -a[i];
  return Point(result);
}

inline Point operator*(double scale, const Point& a) {
  std::vector<double> result(a.dim());
  for (size_t i = 0; i < a.dim(); ++i)
    result[i] = a[i] * scale;
  return Point(result);
}

inline Point operator/(const Point& a, double scale) {
  return (1 / scale) * a;
}

inline std::ostream& operator<<(std::ostream& os, const Point& x) {
  os << '(';
  for (size_t i = 0; i < x.dim() - 1; ++i) {
    os << x[i] << ", ";
  }
  os << x[x.dim() - 1] << ')';
  return os;
}

} // namespace

#endif // POINT_HPP
