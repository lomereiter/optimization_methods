#ifndef STATE_SEQUENCE_FACTORY_HPP
#define STATE_SEQUENCE_FACTORY_HPP

#include "condition.hpp"
#include "sequence.hpp"

#include <memory>

using util::Sequence;

template <typename Generator>
class StateSequenceFactory {
public:
  typedef optimization::StopCondition StopCondition;
private:
  std::shared_ptr<StopCondition> stop_condition_;
public:
  StateSequenceFactory() : stop_condition_(nullptr) {}

  void setStopCondition(const std::shared_ptr<StopCondition> &cond) {
    stop_condition_ = cond;
  }

  typename Sequence<typename Generator::State>::ptr
  make(typename Generator::Args args) {
    stop_condition_->prepareArgs(args);
    auto seq = Generator::run(args);
    return stop_condition_->map(seq);
  }
};

#endif // STATE_SEQUENCE_FACTORY_HPP
