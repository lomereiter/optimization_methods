#ifndef DOMAIN_HPP
#define DOMAIN_HPP

#include "point.hpp"
#include "exception.hpp"
#include <limits>
#include <array>
#include <cstddef>
#include <cmath>

namespace util {

class Interval {
  double from_;
  double to_;
public:
  Interval() :
    from_(-std::numeric_limits<double>::infinity()),
    to_(std::numeric_limits<double>::infinity())
  {}

  Interval(double from, double to) : from_(from), to_(to) {
    if (from_ > to_)
      std::swap(from_, to_);
  }

  double from() const { return from_; }
  double to() const { return to_; }
  double length() const { return to_ - from_; }
};

class Domain {
  std::vector<Interval> proj_;
public:
  Domain(size_t dim) : proj_(dim) {}
  Domain(std::vector<Interval> projections) : proj_(projections) {}

  size_t dim() const {
    return proj_.size();
  }
  
  Interval operator[](size_t index) const {
    return proj_[index];
  }

  bool contains(const Point& point) const {
    if (dim() != point.dim())
      throw new DimensionMismatchException("Domain and point have different dimensions");
    for (size_t i = 0; i < dim(); ++i)
      if (point[i] < proj_[i].from() || point[i] > proj_[i].to())
        return false;
    return true;
  }

  bool operator!=(const Domain &other) const {
      if (dim() != other.dim())
          return true;
      constexpr double eps = 1e-8;
      for (size_t i = 0; i < dim(); ++i) {
          if (std::fabs(proj_[i].from() - other.proj_[i].from()) > eps)
              return true;
          if (std::fabs(proj_[i].to() - other.proj_[i].to()) > eps)
              return true;
      }
      return false;
  }
};

} // namespace

#endif // DOMAIN_HPP
