#ifndef TRAJECTORYVIEW_H
#define TRAJECTORYVIEW_H

#include <QtWidgets/QWidget>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QGraphicsScene>

class TrajectoryView : public QGraphicsView
{
    Q_OBJECT
public:
    explicit TrajectoryView(QGraphicsScene *scene, QWidget *parent);

    virtual void mousePressEvent(QMouseEvent *event);

signals:
    void clicked(QPoint point);
};

#endif // TRAJECTORYVIEW_H
