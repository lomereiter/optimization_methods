#ifndef OPTIMIZATION_GUI_HPP
#define OPTIMIZATION_GUI_HPP

#include "optimizer.hpp"
#include "sequence.hpp"
#include "point.hpp"
#include "condition.hpp"
#include "state_sequence_factory.hpp"

#include <memory>
#include <cassert>

#include <QRect>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QGraphicsPathItem>
#include <QtWidgets/QMessageBox>
#include <QDebug>

namespace gui {

class OptimizationMethodRunner {
public:
  typedef util::Domain Domain;
  typedef std::function<double(double, double)> Function;
  typedef std::shared_ptr<optimization::StopCondition> StopConditionPtr;

protected:
  QGraphicsScene *scene_;

  double best_x_;
  double best_y_;
  double best_f_;

  uint w_;
  uint h_;

  Function current_function_;
  Domain current_domain_;

  StopConditionPtr stop_condition_;

  size_t n_steps_;
  bool finished_;

  double getX(double x) const {
    return w_ * (x - current_domain_[0].from()) / current_domain_[0].length();
  }

  double getY (double y) const {
      return h_ * (y - current_domain_[1].from()) / current_domain_[1].length();
  }

  // must update best_x_, best_y_, and best_f_
  // can draw something on the scene if it wants
  // returns whether next step can be done
  virtual bool makeNextStep() = 0;

  virtual void runImpl(double x, double y) = 0;

public:
  OptimizationMethodRunner() : scene_(NULL), current_function_(), current_domain_(2) {}

  OptimizationMethodRunner* setGraphicsScene(QGraphicsScene *scene) {
    scene_ = scene;
    w_ = scene_->width();
    h_ = scene_->height();
    return this;
  }

  OptimizationMethodRunner* setFunction(Function f) {
    current_function_ = f;
    return this;
  }

  OptimizationMethodRunner* setDomain(const Domain& d) {
    current_domain_ = d;
    return this;
  }

  OptimizationMethodRunner* setStopCondition(StopConditionPtr cond) {
    stop_condition_ = cond;
    return this;
  }

  double bestX() const { return best_x_; }
  double bestY() const { return best_y_; }
  double bestValue() const { return best_f_; }

  uint stepsDone() const { return n_steps_; }

  virtual void initGraphicsScene() = 0;
  virtual void clearGraphicsScene() = 0;

  QPen getCurrentSegmentPen() const {
    double t = 1 - exp(-0.02 * n_steps_);
    uint r = 255 * (1 - sqrt(t));
    uint g = 0;
    uint b = 255 * sqrt(t);
    return QPen(QBrush(QColor(r, g, b)), 3 * (1 - t) + 7 * t);
  }

  void updateGraphicsScene() {
    auto last_best_x = best_x_;
    auto last_best_y = best_y_;

    // updates best_x_ and best_y_
    if (!makeNextStep())
      finished_ = true;

    if (n_steps_ > 0) {
        scene_->addLine(getX(last_best_x), getY(last_best_y),
                        getX(best_x_), getY(best_y_),
                        getCurrentSegmentPen());
    }

    ++n_steps_;
  }

  /// xrel, yrel from range [0, 1]
  void run(double xrel, double yrel) {
    n_steps_ = 0;
    finished_ = false;
    auto &domain = current_domain_;
    auto x = domain[0].from() + domain[0].length() * xrel;
    auto y = domain[1].from() + domain[1].length() * yrel;
    runImpl(x, y);
  }

  bool finished() const {
    return finished_;
  }

  virtual ~OptimizationMethodRunner() {}
};

class NelderMeadOptimizationMethodRunner : public OptimizationMethodRunner {
  QGraphicsPathItem *current_simplex_item_;

  typedef optimization::NelderMeadGenerator Generator;
  typedef Generator::State State;
  typedef util::Sequence<State>::ptr StateSequence;
  StateSequence current_seq_;
public:

  NelderMeadOptimizationMethodRunner() : OptimizationMethodRunner(),
    current_simplex_item_(NULL) {}

  virtual void initGraphicsScene() {
    qDebug() << "#initGraphicsScene";
    if (scene_ == NULL) {
      qDebug() << "   scene is not initialized";
      return;
    }
    current_simplex_item_ = new QGraphicsPathItem();
    auto red_pen = QPen(QBrush(Qt::red), 2);
    current_simplex_item_->setPen(red_pen);
    current_simplex_item_->setPath(QPainterPath());

    scene_->addItem(current_simplex_item_);
    qDebug() << "   created current_simplex_item_";
    qDebug() << current_simplex_item_->scene();
  }

  virtual void clearGraphicsScene() {
    qDebug() << "#clearGraphicsScene";
    current_simplex_item_ = NULL; // FIXME: should it be freed?
  }

protected:
  virtual bool makeNextStep() {
    assert(scene_ != NULL && current_simplex_item_ != NULL);
    assert(current_seq_ != NULL);
    State state = current_seq_->front();
    auto point = state.bestPoint();
    best_x_ = point[0];
    best_y_ = point[1];
    best_f_ = state.valueAtBestPoint();

    QPainterPath path;
    uint xs[3];
    uint ys[3];
    uint x, y;
    for (uint i = 0; i < 3; i++) {
        x = xs[i] = getX(state.getSimplex()[i][0]);
        y = ys[i] = getY(state.getSimplex()[i][1]);

        if (i == 0) {
            path.moveTo(x, y);
        } else {
            if (i == 2 && xs[0] == xs[1] && xs[1] == xs[2] && ys[0] == ys[1] && ys[1] == ys[2]) {
                x += 1; // dumb workaround to make Qt draw the path
            }
            path.lineTo(x, y);
        }
    }
    path.closeSubpath();
    current_simplex_item_->setPath(path);

    scene_->update();

    if (current_seq_->empty())
      return false;
    current_seq_->popFront();
    return true;
  }

  virtual void runImpl(double x, double y) {
    Generator::Args args{
       current_domain_,
       {
         2,
         [&](const util::Point &point) {
           return current_function_(point[0], point[1]);
         }
       },
       std::vector<double>{x, y}};

    StateSequenceFactory<Generator> factory;
    factory.setStopCondition(stop_condition_);
    current_seq_ = factory.make(args);
  }
};

class RandomSearchOptimizationMethodRunner : public OptimizationMethodRunner {
  typedef optimization::RandomSearchGenerator Generator;
  typedef Generator::State State;
  typedef util::Sequence<State>::ptr StateSequence;
  StateSequence current_seq_;
public:

  RandomSearchOptimizationMethodRunner() : OptimizationMethodRunner() {}

  virtual void initGraphicsScene() {}
  virtual void clearGraphicsScene() {}

protected:
  virtual bool makeNextStep() {
    assert(current_seq_ != NULL);
    State state = current_seq_->front();
    auto point = state.bestPoint();
    best_x_ = point[0];
    best_y_ = point[1];
    best_f_ = state.valueAtBestPoint();
    if (current_seq_->empty())
      return false;
    current_seq_->popFront();
    return true;
  }

  virtual void runImpl(double x, double y) {
    Generator::Args args{
      current_domain_,
      {
        2,
        [&](util::Point point) {
          return current_function_(point[0], point[1]);
        }
      },
      0.01,
      0.99,
      std::vector<double>{x, y}};

    StateSequenceFactory<Generator> factory;
    factory.setStopCondition(stop_condition_);
    current_seq_ = factory.make(args);
  }
};

typedef std::shared_ptr<OptimizationMethodRunner> OptimizationMethodRunnerPtr;

} // namespace gui

#endif // OPTIMIZATION_GUI_HPP
