#include <QtWidgets/QApplication>
#include <QCoreApplication>
#include "mainwindow.h"
#include "domain.hpp"
#include "condition.hpp"
#include "optimizer.hpp"
#include "jsfunction.hpp"
#include "state_sequence_factory.hpp"
#include "optimization_gui.hpp"

#include <QDebug>
#include <QtScript/QScriptEngine>
#include <QTextStream>
#include <QSettings>
#include <QRegExp>
#include <fstream>

template <typename Generator> int consoleMain(QSettings &);

using namespace optimization;
using namespace util;

int main(int argc, char *argv[]) {
    bool gui_mode = true;

    QTextStream out(stdout);

    if (argc > 2) {
        out << "Too many command-line arguments!\n"
            "Run with --help to see available options.\n";
        return -1;
    }

    if (argc == 2) {
        QString flag(argv[1]);
        if (flag == "--help") {
            out << "Run with no arguments to start the application in GUI mode\n"
                   "If --console flag is provided, the config file optimization.ini is read,\n"
                   "and the output is written to optimization.out\n";
            return 0;
        } else if (flag == "--console") {
            gui_mode = false;
        } else {
            out << "Couldn't parse command-line flags.\n"
                   "Run with --help to see available options.\n";
            return -1;
        }
    }

    if (gui_mode) {
        QApplication a(argc, argv);
        MainWindow w(1200, 600);
        w.show();
        return a.exec();
    }

    QCoreApplication console_app(argc, argv);

    // console mode
    QSettings settings("optimization.ini", QSettings::IniFormat);
    if (settings.status() != QSettings::NoError) {
        out << "Failed to parse optimization.ini file\n";
        return -1;
    }
    auto method = settings.value("method").toString();
    if (method == "nelder-mead") {
        return consoleMain<NelderMeadGenerator>(settings);
    } else if (method == "random-search") {
        return consoleMain<RandomSearchGenerator>(settings);
    } else {
        out << "Unknown optimization method\n"
               "Supported values of 'method' are 'nelder-mead' and 'random-search'\n";
        return 1;
    }
}

Domain parseDomain(const QString &domain) {
    auto intervals = domain.split("x");
    QRegExp interval_regexp{R"III(^\s*\[\s*(-?\d+(?:\.\d+)?)\s*,\s*(-?\d+(?:\.\d+)?)\s*\]\s*$)III"};

    std::vector<Interval> projs;
    for (const QString & interval : intervals) {
        if (interval_regexp.exactMatch(interval)) {
            auto nums = interval_regexp.capturedTexts();
            projs.emplace_back(nums[1].toDouble(), nums[2].toDouble());
        } else {
            throw "Couldn't parse domain!";
        }
    }
    return Domain{projs};
}

Point parsePoint(const Domain &domain, const QString &point) {
    auto s = point.trimmed();
    if (s.length() == 0 || s[0] != '(' || s[s.length() - 1] != ')')
        throw "Can't parse start point";
    auto scoords = s.mid(1, s.length() - 2).split(',');
    std::vector<double> coords;
    for (auto &c: scoords)
        coords.push_back(c.trimmed().toDouble());
    if (coords.size() != domain.dim())
        throw "Start point has wrong dimension";
    Point result{coords};
    if (!domain.contains(result))
        throw "Start point doesn't belong to the specified domain";
    return result;
}

template <typename Generator> struct ConsoleApp;

template <>
struct ConsoleApp<RandomSearchGenerator> {
    typedef RandomSearchGenerator Gen;
    typedef StateSequenceFactory<Gen> Factory;
    typedef typename Sequence<Gen::State>::ptr StateSeq;
    StateSeq states;

    ConsoleApp(QSettings &settings, Factory &factory, Function &func, Domain &domain) :
        states{factory.make({
            domain,
            func,
            settings.value("random-search/local-domain-percentage").toDouble(),
            settings.value("random-search/local-domain-probability").toDouble(),
            parsePoint(domain, settings.value("start-point").toString())
        })}
    {
    }

    int run() {
        std::ofstream out{"optimization.out"};

        while (!states->empty()) {
            auto current_state = states->front();
            out << current_state.bestPoint() << " => "
                << current_state.valueAtBestPoint() << "\n";
            states->popFront();
        }

        return 0;
    }
};

template <>
struct ConsoleApp<NelderMeadGenerator> {
    typedef NelderMeadGenerator Gen;
    typedef StateSequenceFactory<Gen> Factory;
    typedef typename Sequence<Gen::State>::ptr StateSeq;
    StateSeq states;

    ConsoleApp(QSettings &settings, Factory &factory, Function &func, Domain &domain) :
        states{factory.make({
            domain,
            func,
            parsePoint(domain, settings.value("start-point").toString())
        })}
    {}

    int run() {
        std::ofstream out{"optimization.out"};

        while (!states->empty()) {
            auto current_state = states->front();
            out << current_state.bestPoint() << " => "
                << current_state.valueAtBestPoint() << "\n";

            auto &simplex = current_state.getSimplex();
            out << "  simplex: [\n";
            for (size_t i = 0; i < simplex.size(); ++i)
                out << "    " << simplex[i] << " => " << current_state.values()[i] << "\n";
            out << "  ]\n";

            states->popFront();
        }

        return 0;
    }
};

template <typename Generator>
int consoleMain(QSettings &settings) {
    QTextStream outerr(stderr);
    try {
    StateSequenceFactory<Generator> factory;
    factory.setStopCondition(std::make_shared<CompositeStopCondition>(
                std::make_shared<CompositeStopCondition>(
                    std::make_shared<LimitNumberOfEvaluations>(
                        settings.value("max-evals", 10000).toInt()),
                    std::make_shared<LimitNumberOfSteps>(
                        settings.value("max-steps", 500).toInt())),
                std::make_shared<LimitTime>(
                    std::chrono::seconds(settings.value("time-limit", 5).toInt()))));

    QScriptEngine js_engine;

    auto domain = parseDomain(settings.value("domain").toString());
    Function func{
        domain.dim(),
        JsFunction{settings.value("function").toString(), js_engine}
    };

    return ConsoleApp<Generator>(settings, factory, func, domain).run();
    } catch (const char* msg) {
        outerr << "Error: " << msg << "\n";
        return -1;
    } catch (...) {
        outerr << "Error" << "\n";
        return -1;
    }
}
