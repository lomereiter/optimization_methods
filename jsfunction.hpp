#ifndef JS_FUNC_HPP
#define JS_FUNC_HPP

#include <QtScript/QScriptEngine>
#include "point.hpp"

struct JsFunction {
    QScriptEngine &engine;
    QScriptValueList args;
    QScriptValue js_func2;
    QScriptValue js_func;

    JsFunction(QString func_body, QScriptEngine &js_engine) : engine(js_engine) {
        auto func2 = "(function(x, y) { with(Math) { return " + func_body + "; }})";
        js_func2 = js_engine.evaluate(func2);

        auto func = "(function(x) { with(Math) { return " + func_body + "; }})";
        js_func = js_engine.evaluate(func);
    }

    double operator()(double x, double y) {
        args.clear();
        args << QScriptValue(x) << QScriptValue(y);
        return js_func2.call(QScriptValue(), args).toNumber();
    }

    double operator()(const util::Point &x) {
        auto js_x = engine.newArray(x.dim());
        for (size_t i = 0; i < x.dim(); ++i)
            js_x.setProperty(i, QScriptValue(x[i]));
        args.clear();
        args << js_x;
        return js_func.call(QScriptValue(), args).toNumber();
    }
};

#endif
