#ifndef SEQUENCE_HPP
#define SEQUENCE_HPP

#include <memory>
#include <functional>

namespace util {

template <typename T>
class Sequence {
public:
  typedef T elem_type;
  virtual elem_type front() = 0;
  virtual bool empty() = 0;
  virtual void popFront() = 0;

  typedef std::shared_ptr<Sequence<elem_type> > ptr;

  Sequence() {}
  virtual ~Sequence() {}

  Sequence(const Sequence&) = delete;
};

}

#endif // SEQUENCE_HPP
