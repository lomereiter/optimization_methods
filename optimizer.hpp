#ifndef OPTIMIZER_HPP
#define OPTIMIZER_HPP

#include "point.hpp"
#include "domain.hpp"
#include "sequence.hpp"

#include <functional>
#include <cstddef>
#include <cmath>
#include <limits>
#include <memory>
#include <array>
#include <random>

namespace optimization {

using util::Point;
using util::Sequence;
using util::Domain;

struct Function {
  size_t dim;
  std::function<double(const Point &)> f;

  double operator()(const Point & point) {
    if (point.dim() != dim)
      throw new DimensionMismatchException("point and function have different dimensions");
    return f(point);
  }
};


template <typename Args>
void checkArguments(Args &args) {
  if (args.domain.dim() != args.function.dim)
    throw new DimensionMismatchException(
        "Dimensions of domain and function are different");
  if (args.domain.dim() != args.start_point.dim())
    throw new DimensionMismatchException(
        "Dimensions of function and start point are different");
}

class NelderMeadGenerator {
  class StateSequence;
public:

  class State {
    friend class StateSequence;

    std::vector<Point> simplex;
    std::vector<double> f;
    size_t min_index;
    size_t max1_index;
    size_t max2_index;

    void recalculateIndices() {
      min_index = max1_index = 0;
      for (size_t i = 0; i < simplex.size(); i++) {
        if (f[min_index] > f[i])
          min_index = i;
        if (f[max1_index] < f[i])
          max1_index = i;
      }

      max2_index = min_index;

      for (size_t i = 0; i < simplex.size(); i++) {
        if (i == max1_index)
            continue;
        if (f[max2_index] < f[i])
          max2_index = i;
      }
    }

    Point findCenter() const {
      auto dim = simplex.size() - 1;
      Point center(dim);
      for (size_t i = 0; i < simplex.size(); ++i) {
        if (i == max1_index)
          continue;
        center = center + simplex[i];
      }
      return center / (simplex.size() - 1);
    }

  public:
    State(size_t dim) :
      simplex(dim + 1, Point(dim)), f(dim + 1) {}

    Point bestPoint() const {
      return simplex[min_index];
    }

    double valueAtBestPoint() const {
      return f[min_index];
    }

    const std::vector<Point> &getSimplex() const {
        return simplex;
    }

    const std::vector<double> &values() const {
        return f;
    }
  };

private:
  class StateSequence : public Sequence<State> {
    State state_;
    Function f_;
    Domain domain_;
    Point start_point_;
    
    std::random_device rd_;
    std::mt19937 gen_;

    bool inited_;
    void init() {
      inited_ = true;
      state_.simplex[0] = start_point_;
      state_.f[0] = f_(start_point_);
      for (size_t i = 1; i < domain_.dim() + 1; ++i) {
        generatePoint(i);
      }
      state_.recalculateIndices();
    }

    void generatePoint(size_t index) {
      for (size_t j = 0; j < domain_.dim(); ++j) {
        std::uniform_real_distribution<> dis(domain_[j].from(),
                                             domain_[j].to());
        state_.simplex[index][j] = dis(gen_);
      }
      state_.f[index] = f_(state_.simplex[index]);
    }

    void updateSimplex(const Point &center, const Point &new_point,
                       double new_value)
    {
      Point p = new_point;
      if (!domain_.contains(p)) {
        // find point on the boundary in the direction center -> new point
        double gamma = std::numeric_limits<double>::infinity();
        for (size_t i = 0; i < domain_.dim(); ++i) {
          const auto &interval = domain_[i];
          double x_b_i = center[i] < p[i] ? interval.to() : interval.from();
          double rel = (x_b_i - center[i]) / (p[i] - center[i]);
          if (rel < gamma)
            gamma = rel;
        }
        p = center + gamma * (p - center);
        new_value = f_(p);
      }

      state_.simplex[state_.max1_index] = p;
      state_.f[state_.max1_index] = new_value;
      state_.recalculateIndices();
    }

    void shrinkSimplex() {
      size_t l = state_.min_index;
      auto &points = state_.simplex;
      for (size_t i = 0; i < points.size(); ++i) {
        if (i != l) {
          points[i] = points[l] + (points[i] - points[l]) / 2;
          state_.f[i] = f_(points[i]);
        }
      }
      state_.recalculateIndices();
    }
    
  public:
    StateSequence(const Domain& domain, Function f,
                  const Point &start_point) :
      state_(domain.dim()), f_(f), domain_(domain),
      start_point_(start_point), rd_(),
      gen_(rd_()), inited_(false)
    {
    }
    
    bool empty() { return false; }

    State front() { if (!inited_) init(); return state_; }

    void popFront() {
      if (!inited_) init();

      const double alpha = 1.0;
      const double beta = -0.5;
      const double gamma = 2.0;

      typedef Point P;

      // worst point
      P x_h = state_.simplex[state_.max1_index];
      double f_h = state_.f[state_.max1_index];

      // second worst
      double f_g = state_.f[state_.max2_index];

      // current best
      double f_l = state_.f[state_.min_index];

      // center of gravity
      P x_c = state_.findCenter();

      // reflect the worst point
      P x_r = (1 + alpha) * x_c - alpha * x_h;
      double f_r = f_(x_r);

      // better than the current best?
      if (f_r < f_l) {
        // try to move further in that direction
        P x_e = (1 + gamma) * x_c - gamma * x_h;
        double f_e = f_(x_e);

        if (f_e < f_r) {
          // found even better point
          updateSimplex(x_c, x_e, f_e);
        } else {
          updateSimplex(x_c, x_r, f_r);
        }
      } else if (f_l <= f_r && f_r < f_g) {
        // better than the second worst
        updateSimplex(x_c, x_r, f_r);
      } else {
        if (f_g <= f_r && f_r < f_h) {
          std::swap(x_r, x_h);
          std::swap(f_r, f_h);
        }

        // contracted point
        P x_s = (1 + beta) * x_c - beta * x_h;
        double f_s = f_(x_s);
        if (f_s < f_h) {
          // better? update
          updateSimplex(x_c, x_s, f_s);
        } else {
          // failed to replace the worst point
          shrinkSimplex();
        }
      }
    }
  };

public:

  struct Args {
    Domain domain;
    Function function;
    Point start_point;
  };

  static
  typename Sequence<State>::ptr
  run(const Args &args) {
    return std::make_shared<StateSequence>(args.domain, args.function,
                                           args.start_point);
  }
};

class RandomSearchGenerator {
public:

  class State {
    Point point_;
    double value_;
  public:
    State(size_t dim) :
      point_(dim),  value_(std::numeric_limits<double>::quiet_NaN()) {}

    State(Point point, double value) :
      point_(point), value_(value) {}

    const Point &bestPoint() const {
      return point_;
    }

    double valueAtBestPoint() const {
      return value_;
    }
  };

private:
  class StateSequence : public Sequence<State> {
    Function f_;
    Domain domain_;

    std::random_device rd_;
    std::mt19937 gen_;

    typedef util::Interval Interval;

    double percentage_; // single-dimension percentage
    double probability_; // of local search

    Interval getInterval(const Interval& full, double coord) const {
      double length = full.length() * percentage_;
      double start = coord - length / 2;
      double end = coord + length / 2;
      if (start < full.from()) {
        start = full.from();
        end = start + length;
      } else if (end > full.to()) {
        end = full.to();
        start = end - length;
      }
      return Interval(start, end);
    }

    Domain localDomain() const {
      std::vector<Interval> intervals(domain_.dim());
      for (size_t i = 0; i < domain_.dim(); ++i)
        intervals[i] = getInterval(domain_[i], point_[i]);
      return Domain(intervals);
    }

    void generatePoint(const Domain& domain) {
      for (size_t i = 0; i < domain.dim(); ++i) {
        const auto &range = domain[i];
        std::uniform_real_distribution<> d(range.from(), range.to());
        point_[i] = d(gen_);
      }
      f_value_ = f_(point_);
    }

    Point point_;
    double f_value_;

    public:
    StateSequence(const Domain &domain, Function f,
                  double percentage, double p,
                  const Point &start_point) :
      f_(f), domain_(domain), rd_(), gen_(rd_()),
      percentage_(pow(percentage, 1.0/domain.dim())), probability_(p),
      point_(start_point), f_value_(f_(point_))
    {
    }

    virtual bool empty() {
      return false;
    }

    virtual State front() {
      return State(point_, f_value_);
    }

    virtual void popFront() {
      auto current_value = f_value_;
      std::bernoulli_distribution d{probability_};
      auto local_domain = localDomain();
      while (true) {
        generatePoint(d(gen_) ? local_domain : domain_);

        if (f_value_ < current_value)
          return;
      }
    }
  };

public:

  struct Args {
    Domain domain;
    Function function;
    double local_percentage;
    double local_probability;

    Point start_point;
  };

  static
  typename Sequence<State>::ptr
  run(const Args &args) {
    checkArguments(args);
    return std::make_shared<StateSequence>(args.domain, args.function,
                                           args.local_percentage,
                                           args.local_probability,
                                           args.start_point);
  }
};

} // namespace

#endif // OPTIMIZER_HPP
